'''
ExpenseCategory:

Retrieve: SELECT * FROM expensecategory WHERE owner_id = :user_id;
Insert: INSERT INTO expensecategory (name, owner_id) VALUES (:name, :owner_id) RETURNING id;
'''


from pydantic import BaseModel
from queries.pool import pool

class CategoryIn(BaseModel):
    name: str
    owner_id: int

class CategoryOut(BaseModel):
    id: int
    name: str
    owner_id: int


class CategoryQueries:
    def get_all_category_for_user(
          self, user_id: int
    ) -> list[CategoryOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                     SELECT * 
                     FROM expensecategory
                     WHERE owner_id = %s;
                    """,
                    [user_id],
                )
                try:
                    results = []
                    for row in cur.fetchall():
                        record = {}
                        for i, column in enumerate(cur.description):
                            record[column.name] = row[i]
                        results.append(record)
                    return results
                except Exception as e:
                    print(e)
                    return {
                        "message": "Could not get expense category records for this user id"
                    }

