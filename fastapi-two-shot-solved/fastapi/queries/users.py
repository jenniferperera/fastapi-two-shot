
'''
Users:

Retrieve: SELECT * FROM users WHERE id = :user_id;
Insert: INSERT INTO users (password, username, first_name, last_name, email, date_joined) VALUES (:password, :username, :first_name, :last_name, :email, NOW()) RETURNING id;
'''

from pydantic import BaseModel
from queries.pool import pool
from datetime import date

class DuplicateUserError(ValueError):
    pass

class UserIn(BaseModel):
    username: str
    first_name: str
    last_name: str
    email: str
    password: str


class UserOut(BaseModel):
    id: int
    username: str
    first_name: str
    last_name: str
    email: str

class UserOutWithPassword(UserOut):
    hashed_password: str

class UserQueries:
    def get(
          self, user_email: str
    ) -> UserOutWithPassword:
        print("here in get): " +user_email)
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                     SELECT * 
                     FROM users
                     WHERE email = %s;
                    """,
                    [user_email],
                )
                try:
                    record = None
                    for row in cur.fetchall():
                        record = {}
                        for i, column in enumerate(cur.description):
                            record[column.name] = row[i]
                    return UserOutWithPassword(**record)
                except Exception:
                    print("exception")
                    return {
                        "message": "Could not get user record for this user email"
                    }
# Insert: INSERT INTO users (password, username, first_name, last_name, email, date_joined) VALUES (:password, :username, :first_name, :last_name, :email, NOW()) RETURNING id;
    def create_user(self, data, hashed_password) ->UserOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.first_name,
                    data.last_name,
                    data.username,
                    data.email,
                    hashed_password
                ]
                cur.execute(
                    """
                    INSERT INTO users (first_name, last_name, username, email,
                    hashed_password)
                    VALUES (%s, %s, %s, %s, %s)
                    RETURNING id, first_name, last_name, username,email,
                    hashed_password
                    """,
                    params,
                )

                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                print(record)
                return UserOutWithPassword(**record) 
