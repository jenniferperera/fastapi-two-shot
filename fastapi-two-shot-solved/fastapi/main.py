import os
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import account, category, users, receipt
from authenticator import authenticator

app = FastAPI()

# CORS middleware for frontend communication
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/")
def read_root():
    return {"Hello": "World"}

# ... More routes will be added here ...
#git test




app.include_router(account.router, tags=["Accounts"])
app.include_router(category.router,tags=["Cat"])
app.include_router(users.router, tags=["AUTH"])
app.include_router(receipt.router, tags=["Receipts"])
app.include_router(authenticator.router, tags=["AUTH"])


