from fastapi import APIRouter, Depends
from typing import Optional, Union
from queries.receipt import (
    ReceiptIn,
    ReceiptOut,
    ReceiptQueries,
    Error,
)
from datetime import date


router = APIRouter()






@router.get("/receipts/{user_id}", response_model=list[ReceiptOut])
async def get_all_receipts(
    user_id: int,
    repo: ReceiptQueries = Depends(),
):
    return repo.get_all_receipt_for_user(user_id)





@router.post("/receipts/")
async def create_receipt():
    return "tbd"