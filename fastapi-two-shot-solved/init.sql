DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS expensecategory;
DROP TABLE IF EXISTS account;
DROP TABLE IF EXISTS receipt;




-- Users table (representing Django's auth_user)
CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY,
    hashed_password VARCHAR(128),
    last_login TIMESTAMP WITH TIME ZONE,
    username VARCHAR(150) UNIQUE NOT NULL,
    first_name VARCHAR(30),
    last_name VARCHAR(30),
    email VARCHAR(254),
    date_joined TIMESTAMP WITH TIME ZONE
    -- Add any other fields you might need
);

CREATE TABLE IF NOT EXISTS expensecategory (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50),
    owner_id INTEGER REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS account (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    number VARCHAR(20),
    owner_id INTEGER REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS receipt (
    id SERIAL PRIMARY KEY,
    vendor VARCHAR(200),
    total DECIMAL(10,3),
    tax DECIMAL(10,3),
    date TIMESTAMP,
    purchaser_id INTEGER REFERENCES users(id) ON DELETE CASCADE,
    category_id INTEGER REFERENCES expensecategory(id) ON DELETE CASCADE,
    account_id INTEGER REFERENCES account(id) ON DELETE SET NULL
);


-------- SEED DATA ----------
-- Users
INSERT INTO users (hashed_password, username, first_name, last_name, email, date_joined) VALUES
('password123', 'alex_riley', 'Alex', 'Riley', 'alex.riley@email.com', NOW()),
('password123', 'maya_patel', 'Maya', 'Patel', 'maya.patel@email.com', NOW()),
('password123', 'chris_wong', 'Chris', 'Wong', 'chris.wong@email.com', NOW()),
('password123', 'olivia_martinez', 'Olivia', 'Martinez', 'olivia.martinez@email.com', NOW()),
('password123', 'ethan_kim', 'Ethan', 'Kim', 'ethan.kim@email.com', NOW()),
('password123', 'sophia_lee', 'Sophia', 'Lee', 'sophia.lee@email.com', NOW()),
('password123', 'jackson_brown', 'Jackson', 'Brown', 'jackson.brown@email.com', NOW()),
('password123', 'lily_johnson', 'Lily', 'Johnson', 'lily.johnson@email.com', NOW()),
('password123', 'michael_davis', 'Michael', 'Davis', 'michael.davis@email.com', NOW()),
('password123', 'emma_garcia', 'Emma', 'Garcia', 'emma.garcia@email.com', NOW());

-- ExpenseCategory
INSERT INTO expensecategory (name, owner_id) VALUES
('Dining Out', 1),
('Groceries', 1),
('Travel', 1),
('Dining Out', 2),
('Entertainment', 2),
('Shopping', 2),
('Travel', 3),
('Dining Out', 3),
('Entertainment', 3),
('Groceries', 4),
('Shopping', 4),
('Travel', 4),
('Dining Out', 5),
('Entertainment', 5),
('Groceries', 5),
('Shopping', 6),
('Travel', 6),
('Dining Out', 6),
('Entertainment', 7),
('Groceries', 7),
('Shopping', 7),
('Travel', 8),
('Dining Out', 8),
('Entertainment', 8),
('Groceries', 9),
('Shopping', 9),
('Travel', 9),
('Dining Out', 10),
('Entertainment', 10),
('Groceries', 10);

-- Account
INSERT INTO account (name, number, owner_id) VALUES
('Savings', '1234567890', 1),
('Checking', '0987654321', 1),
('Savings', '1122334455', 2),
('Checking', '5566778899', 2),
('Savings', '2233445566', 3),
('Checking', '6677889900', 3),
('Savings', '3344556677', 4),
('Checking', '7788990011', 4),
('Savings', '4455667788', 5),
('Checking', '8899001122', 5),
('Savings', '5566778899', 6),
('Checking', '9900112233', 6),
('Savings', '6677889900', 7),
('Checking', '0011223344', 7),
('Savings', '7788990011', 8),
('Checking', '1122334455', 8),
('Savings', '8899001122', 9),
('Checking', '2233445566', 9),
('Savings', '9900112233', 10),
('Checking', '3344556677', 10);

-- Receipt
INSERT INTO receipt (vendor, total, tax, date, purchaser_id, category_id, account_id) VALUES
('Starbucks', 5.50, 0.50, NOW(), 1, 1, 1),
('Amazon', 50.00, 5.00, NOW(), 1, 2, 2),
('Delta Airlines', 250.00, 25.00, NOW(), 1, 3, 1),
('McDonalds', 10.00, 1.00, NOW(), 2, 4, 3),
('Netflix', 15.00, 1.50, NOW(), 2, 5, 4),
('Walmart', 75.00, 7.50, NOW(), 2, 6, 3),
('United Airlines', 300.00, 30.00, NOW(), 3, 7, 5),
('Burger King', 8.00, 0.80, NOW(), 3, 8, 6),
('HBO Max', 14.99, 1.49, NOW(), 3, 9, 5),
('Target', 100.00, 10.00, NOW(), 4, 10, 7);
