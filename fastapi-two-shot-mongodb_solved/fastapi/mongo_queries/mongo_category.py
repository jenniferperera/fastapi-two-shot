"""
categories.py

This module encapsulates the data models and database interactions for managing category-related operations within MongoDB. It leverages PyMongo for database connectivity and operations, and Pydantic for robust data validation and schema enforcement.

Classes:
    MongoCategoryIn - A Pydantic model that defines the schema for new category data to be inserted into the database.
    MongoCategoryOut - A Pydantic model that structures the category data retrieved from the database.
    MongoCategoryQueries - A class that contains methods for CRUD operations on the 'expensecategory' collection in MongoDB.

Usage:
    Instantiate MongoCategoryQueries to perform database operations on the 'expensecategory' collection.
    Utilize MongoCategoryIn to validate and structure new category data before database insertion.
    Employ MongoCategoryOut to parse and structure category data after retrieval from the database.

Example:
    category_queries = MongoCategoryQueries()
    new_category_data = {'name': "Utilities", 'owner_id': "507f1f77bcf86cd799439011"}
    new_category = MongoCategoryIn(**new_category_data)
    inserted_category = category_queries.insert_category(new_category)
    print(inserted_category)
"""



from pydantic import BaseModel
from pymongo.collection import Collection
from pymongo.errors import PyMongoError
from bson import ObjectId
from fastapi import HTTPException
from mongo_queries.client import Queries

class MongoCategoryIn(BaseModel):
    name: str
    owner_id: str  # Assuming owner_id will be the string representation of the ObjectId

class MongoCategoryOut(BaseModel):
    id: str
    name: str
    owner_id: str

class MongoCategoryQueries(Queries):
    DB_NAME = "twoshot"  
    COLLECTION = "expensecategory"


    def get_all_category_for_user(self, user_id: str) -> list[MongoCategoryOut]:
        try:
            owner_object_id = ObjectId(user_id)
            results = self.collection.find({"owner_id": owner_object_id})
            categories = [
                MongoCategoryOut(id=str(category["_id"]), name=category["name"], owner_id=str(category["owner_id"]))
                for category in results
            ]
            return categories
        except PyMongoError as e:
            raise HTTPException(status_code=500, detail=str(e))

    def insert_category(self, category_data: MongoCategoryIn) -> MongoCategoryOut:
        try:
            category_data_dict = category_data.dict(by_alias=True)
            category_data_dict['owner_id'] = ObjectId(category_data_dict['owner_id'])
            result = self.collection.insert_one(category_data_dict)
            category_out = MongoCategoryOut(
                id=str(result.inserted_id),
                name=category_data.name,
                owner_id=str(category_data_dict['owner_id'])  

            )
            return category_out
        except PyMongoError as e:
            raise HTTPException(status_code=500, detail=str(e))

"""
Define MongoCategoryIn Model with fields:
    - name as string
    - owner_id as string representing ObjectId

Define MongoCategoryOut Model with fields:
    - id as string
    - name as string
    - owner_id as string

Define MongoCategoryQueries class inheriting from Queries with:
    - DB_NAME set to "twoshot"
    - COLLECTION set to "expensecategory"

Define method get_all_category_for_user with parameter user_id:
    Convert user_id to ObjectId
    Retrieve all documents from 'expensecategory' collection where 'owner_id' matches
    If retrieval is successful:
        Map each document to MongoCategoryOut model
        Return list of MongoCategoryOut models
    If retrieval fails:
        Raise HTTPException with status 500 and error details

Define method insert_category with parameter category_data:
    Convert category_data to dictionary
    Insert new document into 'expensecategory' collection
    If insertion is successful:
        Create MongoCategoryOut model with inserted data
        Return MongoCategoryOut model
    If insertion fails:
        Raise HTTPException with status 500 and error details
"""