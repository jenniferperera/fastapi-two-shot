from fastapi import APIRouter, Depends, HTTPException
from typing import Optional
from queries.account import (
    AccountOut,
    AccountIn,
    AccountQueries
)

from mongo_queries.mongo_account import (
    MongoAccountQueries,
    MongoAccountIn,
    MongoAccountOut
)
from datetime import date


router = APIRouter()





# @router.get("/accounts/{owner_id}/", response_model=list[AccountOut])
# async def get_account(
#     owner_id: int,
#     repo: AccountQueries = Depends(),
# ):
#     return repo.get_all_accounts_for_user(owner_id)


@router.get("/accounts/{owner_id}/", response_model=list[MongoAccountOut])
async def get_account(
    owner_id: str,
    repo: MongoAccountQueries = Depends(),
) -> list[MongoAccountOut]:
    """
    API endpoint to retrieve all accounts associated with a given user.

    This endpoint will query the database for all accounts linked to the user ID provided in the path parameter. It returns a list of accounts, each represented by a MongoAccountOut object.

    Parameters:
        owner_id: The unique identifier of the user, whose accounts are to be retrieved.
        repo: The repository instance to interact with the database.

    Returns:
        A list of MongoAccountOut objects representing the user's accounts.

    Raises:
        HTTPException: If any error occurs during the database query, an HTTPException is raised with the appropriate status code and detail message.
    """
    # return repo.get_all_accounts_for_user(owner_id)
    accounts = repo.get_all_accounts_for_user(owner_id)
    if isinstance(accounts, list) and accounts and "message" in accounts[0]:
        raise HTTPException(status_code=400, detail=accounts[0]["message"])
    return accounts
    

@router.post("/accounts/", response_model=MongoAccountOut)
async def create_account(
    account_data: MongoAccountIn,
    repo: MongoAccountQueries = Depends(),
) -> MongoAccountOut:
    """
    API endpoint to create a new account.

    Parameters:
        account_data: The data for the new account to be created.
        repo: The repository instance to interact with the database.

    Returns:
        The created account as a MongoAccountOut object.
    """
    try:
        # Insert the account and get the created account object
        account_out = repo.insert_account(account_data)
        # Return the created account object
        return account_out
    except HTTPException as http_exc:
        # If an HTTPException was raised, re-raise it to be handled by FastAPI
        raise http_exc
    except Exception as exc:
        # For any other exceptions, raise a 500 server error
        raise HTTPException(status_code=500, detail=str(exc))