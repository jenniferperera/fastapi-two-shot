from fastapi import APIRouter, Depends, HTTPException, status
from queries.users import (
  UserOut,
  UserIn,
  UserQueries
)

from mongo_queries.mongo_users import ( 
  DuplicateUserError,
  MongoUserQueries,
  MongoUserIn,
  MongoUserOut
)


from datetime import date


router = APIRouter()





# @router.get("/users/{user_id}/", response_model=UserOut)
# async def get_user(
#     user_id: int,
#     repo: UserQueries = Depends(),
# ):
#     return repo.get_user(user_id)

@router.get("/users/{user_id}", response_model=MongoUserOut)
async def get_user(user_id: str, queries: MongoUserQueries = Depends()):
    try:
        return queries.get_by_id(user_id)
    except HTTPException as e:
        # Re-raise the HTTPException caught from the queries class
        raise e
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"An error occurred: {str(e)}")
    

@router.post("/users/", response_model=MongoUserOut, status_code=status.HTTP_201_CREATED)
async def create_user(user_in: MongoUserIn, queries: MongoUserQueries = Depends()):
    try:
        return queries.create(user_in)
    except DuplicateUserError as e:
        raise HTTPException(status_code=400, detail=str(e))
    except HTTPException as e:
        # Re-raise the HTTPException caught from the queries class
        raise e
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"An error occurred: {str(e)}")


@router.delete("/users/{user_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_user(user_id: str, queries: MongoUserQueries = Depends()):
    success = queries.delete(user_id)
    if not success:
        raise HTTPException(status_code=404, detail="User not found")
    return {"ok": True}

@router.get("/users/", response_model=list[MongoUserOut])
async def get_all_users(queries: MongoUserQueries = Depends()):
    try:
        return queries.get_all_users()
    except HTTPException as e:
        # Re-raise the HTTPException caught from the queries class
        raise e
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"An error occurred: {str(e)}")


# Additional CRUD operations would be added here following the same pattern
@router.get("/users/{user_id}", response_model=UserOut)
async def get_user(user_id: int, queries: UserQueries = Depends()):
    try:
        return queries.get_user(user_id)
    except HTTPException as e:
        raise e
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"An error occurred: {str(e)}")

@router.post("/users/", response_model=UserOut, status_code=status.HTTP_201_CREATED)
async def create_user(user_in: UserIn, queries: UserQueries = Depends()):
    try:
        return queries.create(user_in)
    except DuplicateUserError as e:
        raise HTTPException(status_code=400, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"An error occurred: {str(e)}")

@router.delete("/users/{user_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_user(user_id: int, queries: UserQueries = Depends()):
    success = queries.delete(user_id)
    if not success:
        raise HTTPException(status_code=404, detail="User not found")
    return {"ok": True}

@router.get("/users/", response_model=list[UserOut])
async def get_all_users(queries: UserQueries = Depends()):
    try:
        return queries.get_all_users()
    except HTTPException as e:
        raise e
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"An error occurred: {str(e)}")