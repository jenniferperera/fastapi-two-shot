from fastapi import APIRouter, Depends, HTTPException, status
from queries.receipt import (
    ReceiptIn,
    ReceiptOut,
    ReceiptQueries,
    Error,
)
from datetime import date
from bson import ObjectId
from mongo_queries.mongo_receipt import (
    MongoReceiptIn,
    MongoReceiptOut,
    MongoReceiptQueries,
    MongoReceiptOutAGG
)
router = APIRouter()



# @router.get("/receipts/{user_id}", response_model=list[ReceiptOut])
# async def get_all_receipts(
#     user_id: int,
#     repo: ReceiptQueries = Depends(),
# ):
#     return repo.get_all_receipt_for_user(user_id)




@router.get("/receipts/{user_id}", response_model=list[MongoReceiptOutAGG])
async def get_all_receipts_for_user(user_id: str, queries: MongoReceiptQueries = Depends()):
    """
    Retrieve all receipts associated with a given user ID.

    Args:
        user_id (str): The unique identifier of the user whose receipts are to be retrieved.
        queries (MongoReceiptQueries, optional): Dependency that allows interaction with receipt queries. Defaults to Depends().

    Returns:
        list[MongoReceiptOutAGG]: A list of aggregated receipt data including related user, category, and account information.
    """
    return queries.get_all_receipts_for_user(user_id)

@router.get("/receipt/{receipt_id}", response_model=MongoReceiptOut)
async def get_receipt_by_id(receipt_id: str, queries: MongoReceiptQueries = Depends()):
    """
    Retrieve a single receipt by its unique identifier.

    Args:
        receipt_id (str): The unique identifier of the receipt to retrieve.
        queries (MongoReceiptQueries, optional): Dependency that allows interaction with receipt queries. Defaults to Depends().

    Returns:
        MongoReceiptOut: The receipt data corresponding to the provided ID.

    Raises:
        HTTPException: If the receipt_id is not a valid ObjectId or if the receipt is not found.
    """
    try:
        receipt_id_obj = ObjectId(receipt_id)
    except Exception as e:
        raise HTTPException(status_code=400, detail=f"Invalid receipt ID: {e}")
    return queries.get_receipt_by_id(receipt_id_obj)

@router.post("/receipts/", response_model=MongoReceiptOut, status_code=status.HTTP_201_CREATED)
async def create_receipt(receipt_in: MongoReceiptIn, queries: MongoReceiptQueries = Depends()):
    """
    Create a new receipt with the given receipt data.

    Args:
        receipt_in (MongoReceiptIn): The data of the receipt to be created.
        queries (MongoReceiptQueries, optional): Dependency that allows interaction with receipt queries. Defaults to Depends().

    Returns:
        MongoReceiptOut: The created receipt data.
    """
    return queries.create_receipt(receipt_in)

@router.put("/receipts/{receipt_id}", response_model=MongoReceiptOut)
async def update_receipt(receipt_id: str, receipt_in: MongoReceiptIn, queries: MongoReceiptQueries = Depends()):
    """
    Update an existing receipt with the given data.

    Args:
        receipt_id (str): The unique identifier of the receipt to update.
        receipt_in (MongoReceiptIn): The new data for the receipt.
        queries (MongoReceiptQueries, optional): Dependency that allows interaction with receipt queries. Defaults to Depends().

    Returns:
        MongoReceiptOut: The updated receipt data.

    Raises:
        HTTPException: If the receipt_id is not a valid ObjectId.
    """
    try:
        receipt_id_obj = ObjectId(receipt_id)
    except Exception as e:
        raise HTTPException(status_code=400, detail=f"Invalid receipt ID: {e}")
    return queries.update_receipt(receipt_id_obj, receipt_in)

@router.delete("/receipts/{receipt_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_receipt(receipt_id: str, queries: MongoReceiptQueries = Depends()):
    """
    Delete a receipt by its unique identifier.

    Args:
        receipt_id (str): The unique identifier of the receipt to delete.
        queries (MongoReceiptQueries, optional): Dependency that allows interaction with receipt queries. Defaults to Depends().

    Returns:
        dict: A message indicating successful deletion.

    Raises:
        HTTPException: If the receipt_id is not a valid ObjectId.
    """
    try:
        receipt_id_obj = ObjectId(receipt_id)
    except Exception as e:
        raise HTTPException(status_code=400, detail=f"Invalid receipt ID: {e}")
    queries.delete_receipt(receipt_id_obj)
    return {"message": "Receipt deleted successfully"}