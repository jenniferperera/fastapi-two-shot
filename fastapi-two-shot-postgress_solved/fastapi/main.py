import os
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import account, category, users, receipt

app = FastAPI()

# CORS middleware for frontend communication
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/")
def read_root():
    return {"Hello": "World"}

# ... More routes will be added here ...




app.include_router(account.router, tags=["Account"])
app.include_router(category.router, tags=["Category"])
app.include_router(users.router,tags=["Users"])
app.include_router(receipt.router,tags=["Receipt"])


