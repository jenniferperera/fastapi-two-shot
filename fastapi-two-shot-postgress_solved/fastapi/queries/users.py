
'''
Users:

Retrieve: SELECT * FROM users WHERE id = :user_id;
Insert: INSERT INTO users (password, username, first_name, last_name, email, date_joined) VALUES (:password, :username, :first_name, :last_name, :email, NOW()) RETURNING id;
'''

from ast import List
from typing import Optional
from pydantic import BaseModel
from queries.pool import pool
from datetime import date





class UserIn(BaseModel):
    """
    UserIn: Pydantic model for user input data.
    Attributes:
        username (str): Username of the user.
        first_name (str): First name of the user.
        last_name (str): Last name of the user.
        email (str): Email address of the user.
    """
    username: str
    first_name: str
    last_name: str
    email: str

class UserOut(BaseModel):
    """
    UserOut: Pydantic model for user output data.
    Attributes:
        id (int): Unique identifier of the user.
        username (str): Username of the user.
        first_name (str): First name of the user.
        last_name (str): Last name of the user.
        email (str): Email address of the user.
    """
    id: int
    username: str
    first_name: str
    last_name: str
    email: str

class UserCategories(BaseModel):
    username: str
    category_names: Optional[str] = None

class UserCategoriesCount(BaseModel):
    username: str
    category_count: Optional[int] = None

class UserQueries:
    """
    UserQueries: Class containing methods for performing CRUD operations on user data.
    """

    def get_user(self, user_id: int) -> UserOut:
        """
        Retrieves a user by their ID.

        Args:
            user_id (int): The unique identifier of the user.

        Returns:
            UserOut: The user data as a UserOut object.

        Raises:
            Exception: If no user is found with the given ID.
        """
        with pool.connection() as conn:
            with conn.cursor() as cur:
                # SQL query to retrieve user data
                cur.execute(
                    """
                    SELECT id, username, first_name, last_name, email
                    FROM users
                    WHERE id = %s;
                    """,
                    [user_id]
                )
                user = cur.fetchone()
                if user is None:
                    raise Exception("User not found")
                return UserOut(**user)

    def create(self, user_in: UserIn) -> UserOut:
        """
        Creates a new user in the database.

        Args:
            user_in (UserIn): The user data to be inserted.

        Returns:
            UserOut: The created user data as a UserOut object.
        """
        with pool.connection() as conn:
            with conn.cursor() as cur:
                # SQL query to insert new user data
                cur.execute(
                    """
                    INSERT INTO users (username, first_name, last_name, email)
                    VALUES (%s, %s, %s, %s)
                    RETURNING id, username, first_name, last_name, email;
                    """,
                    (user_in.username, user_in.first_name, user_in.last_name, user_in.email)
                )
                user = cur.fetchone()
                user_data = {
                    "id": user[0],
                    "username": user[1],
                    "first_name": user[2],
                    "last_name": user[3],
                    "email": user[4]
                }
                return UserOut(**user_data)


    def update(self, user_id: int, user_update: UserIn) -> UserOut:
        """
        Updates an existing user's data.

        Args:
            user_id (int): The unique identifier of the user to be updated.
            user_update (UserIn): The new data for the user.

        Returns:
            UserOut: The updated user data as a UserOut object, or None if the user does not exist.
        """
        with pool.connection() as conn:
            with conn.cursor() as cur:
                # SQL query to update user data
                cur.execute(
                    """
                    UPDATE users
                    SET username = %s, first_name = %s, last_name = %s, email = %s
                    WHERE id = %s
                    RETURNING id, username, first_name, last_name, email;
                    """,
                    (user_update.username, user_update.first_name, user_update.last_name, user_update.email, user_id)
                )
                user = cur.fetchone()
                if user:
                    user_data = {
                        "id": user[0],
                        "username": user[1],
                        "first_name": user[2],
                        "last_name": user[3],
                        "email": user[4]
                    }
                    return UserOut(**user_data)
                return None

    def delete(self, user_id: int) -> bool:
        """
        Deletes a user from the database.

        Args:
            user_id (int): The unique identifier of the user to be deleted.

        Returns:
            bool: True if the deletion was successful, False otherwise.
        """
        with pool.connection() as conn:
            with conn.cursor() as cur:
                # SQL query to delete user data
                cur.execute(
                    """
                    DELETE FROM users
                    WHERE id = %s;
                    """,
                    [user_id]
                )
                return cur.rowcount > 0

    def get_all_users(self) -> list [UserOut]:
        """
        Retrieves all users from the database.

        Returns:
            List[UserOut]: A list of UserOut objects representing all users.
        """
        with pool.connection() as conn:
            with conn.cursor() as cur:
                # # SQL query to retrieve all user data
                # cur.execute("SELECT * FROM users;")
                # users = [UserOut(**row) for row in cur.fetchall()]
                # return users
                try:
                    cur.execute("SELECT * FROM users;")
                    records = cur.fetchall()
                    return [{column.name: row[i] for i, column in enumerate(cur.description)} for row in records]
                except Exception as e:
                    return {"message": f"Could not retrieve users: {str(e)}"}
                


    def get_user_categories(self) -> list[UserCategories]:
        results = []  # Initialize results before the try block
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        SELECT u.username, ec.name AS category_names
                        FROM users u
                        LEFT JOIN expensecategory ec ON u.id = ec.owner_id;
                        """
                    )
                    rows = cur.fetchall()
                    results = [UserCategories(username=row[0], category_names=row[1]) for row in rows]
        except Exception as e:
            # Handle exceptions (log or print the error)
            print(f"An error occurred: {e}")
        return results

 

    # def get_user_categories(self) -> list[UserCategoriesCount]:
    #     try:
    #         with pool.connection() as conn:
    #             with conn.cursor() as cur:
    #                 cur.execute(
    #                     # """
    #                     # SELECT 
    #                     #     u.username, 
    #                     #     COALESCE(COUNT(ec.name), 0) AS category_count
    #                     # FROM 
    #                     #     users u
    #                     # LEFT JOIN 
    #                     #     expensecategory ec ON u.id = ec.owner_id
    #                     # GROUP BY 
    #                     #     u.username
    #                     # ORDER BY 
    #                     #     u.username;
    #                     # """
    #                     """
    #                     SELECT u.username, 
    #                     COALESCE(COUNT(ec.name), 0)  AS category_count
    #                     FROM users u
    #                     LEFT JOIN expensecategory ec ON u.id = ec.owner_id
    #                     GROUP BY u.id;
    #                 """
    #                 )
    #                 rows = cur.fetchall()

    #                 # Process results using a basic loop
    #                 results = []
    #                 for row in rows:
    #                     username = row[0]
    #                     category_count = row[1]
    #                     results.append(UserCategories(username=username, category_count=category_count))
    #     except Exception as e:
    #         # Handle exceptions (log or print the error)
    #         print(f"An error occurred: {e}")
    #         results = []  # Ensure results is always defined

    #     return results
