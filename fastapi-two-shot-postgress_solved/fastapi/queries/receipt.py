'''
Receipt:

Retrieve: SELECT * FROM receipt WHERE purchaser_id = :user_id;
Insert: INSERT INTO receipt (vendor, total, tax, date, purchaser_id, category_id, account_id) VALUES (:vendor, :total, :tax, NOW(), :purchaser_id, :category_id, :account_id) RETURNING id;
'''

from pydantic import BaseModel
from queries.pool import pool
from datetime import date


class Error(BaseModel):
    message: str

class ReceiptIn(BaseModel):
    vendor: str
    total: float
    tax: float
    purchaser_id: int
    category_id: int
    account_id: int

class ReceiptOut(BaseModel):
    id: int
    vendor: str
    total: float
    tax: float
    date: date
    purchaser_id: int
    category_id: int
    account_id: int


class ReceiptQueries:
    def get_all_receipt_for_user(
          self, user_id: int
    ) -> list[ReceiptOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                     SELECT * 
                     FROM receipt
                     WHERE purchaser_id = %s;
                    """,
                    [user_id],
                )
                try:
                    results = []
                    for row in cur.fetchall():
                        record = {}
                        for i, column in enumerate(cur.description):
                            record[column.name] = row[i]
                        results.append(record)
                    return results
                except Exception as e:
                    print (e)
                    return {
                        "message": "Could not get receipt  records for this user id"
                    }

