"""
accounts.py

This module contains the data models and database query definitions for handling account-related operations in MongoDB.
It uses PyMongo to interact with the database and Pydantic for data validation.

Classes:
    MongoAccountIn - A Pydantic model for account data to be inserted into the database.
    MongoAccountOut - A Pydantic model for account data retrieved from the database.
    MongoAccountQueries - Contains methods for performing CRUD operations on the 'accounts' collection.

Usage:
    Create an instance of MongoAccountQueries to interact with the 'accounts' collection.
    Use MongoAccountIn to validate new account data before insertion.
    Use MongoAccountOut to format account data retrieved from the database.

Example:
    account_queries = MongoAccountQueries()
    new_account_data = {'name': "John Doe", 'number': "123456789", 'owner_id': "507f1f77bcf86cd799439011"}
    new_account = MongoAccountIn(**new_account_data)
    inserted_account = account_queries.insert_account(new_account)
    print(inserted_account)
"""



from pydantic import BaseModel
from pymongo.collection import Collection
from pymongo.errors import PyMongoError
from bson import ObjectId
from fastapi import HTTPException
from mongo_queries.client import Queries

class MongoAccountIn(BaseModel):
    # Pydantic model for incoming account data, with type annotations.
    name: str
    number: str
    owner_id: str  # Expected to be a string representation of ObjectId

class MongoAccountOut(BaseModel):
    # Pydantic model for outgoing account data, with type annotations.
    id: str
    name: str
    number: str
    owner_id: str  # This will be a string representation of ObjectId

class MongoAccountQueries(Queries):
    # Class to handle database operations for accounts.
    DB_NAME = "twoshot"
    COLLECTION = "accounts"

    def get_all_accounts_for_user(self, owner_id):
        """
        Retrieves all accounts associated with a user from the 'accounts' collection.

        Parameters:
            owner_id: The ObjectId of the user in string format.

        Returns:
            A list of MongoAccountOut instances representing the user's accounts.

        Raises:
            HTTPException: An error 500 if the database query fails.
        """
        try:
            accounts_collection = self.collection
            owner_object_id = ObjectId(owner_id)
            results = accounts_collection.find({"owner_id": owner_object_id})
            accounts = [MongoAccountOut(**account) for account in results]
            return accounts
        except PyMongoError as e:
            raise HTTPException(status_code=500, detail=str(e))

    def insert_account(self, account_data: MongoAccountIn) -> MongoAccountOut:
        """
        Inserts a new account into the 'accounts' collection.

        Parameters:
            account_data: An instance of MongoAccountIn representing the account to insert.

        Returns:
            An instance of MongoAccountOut representing the inserted account.

        Raises:
            HTTPException: An error 500 if the insertion fails.
        """
        try:
            # Convert the incoming Pydantic model to a dictionary
            account_data_dict = account_data.dict()
            # Convert the owner_id to an ObjectId
            account_data_dict['owner_id'] = ObjectId(account_data_dict['owner_id'])
            # Insert the account data into the MongoDB collection
            result = self.collection.insert_one(account_data_dict)
            # Retrieve the inserted_id from the result
            inserted_id = result.inserted_id
            # Return a MongoAccountOut instance, ensuring the 'id' field is set correctly
            return MongoAccountOut(
                id=str(inserted_id),  # Convert ObjectId to string
                name=account_data.name,
                number=account_data.number,
                owner_id=account_data.owner_id  # No need to convert again, it's already a string
            )
        except PyMongoError as e:
            raise HTTPException(status_code=500, detail=str(e))

    def get_all_accounts_for_user(self, owner_id: str) -> list[MongoAccountOut]:
        """
        Retrieves all accounts associated with a user from the 'accounts' collection.

        Parameters:
            owner_id: The ObjectId of the user in string format.

        Returns:
            A list of MongoAccountOut instances representing the user's accounts.

        Raises:
            HTTPException: An error 500 if the database query fails.
        """
        try:
            accounts_collection = self.collection
            owner_object_id = ObjectId(owner_id)
            results = accounts_collection.find({"owner_id": owner_object_id})
            # Transform the results to match the MongoAccountOut model
            accounts = []
            for account in results:
                account_data = {
                    'id': str(account['_id']),
                    'name': account['name'],
                    'number': account['number'],
                    'owner_id': str(account['owner_id'])
                }
                accounts.append(MongoAccountOut(**account_data))
            return accounts
        except PyMongoError as e:
            raise HTTPException(status_code=500, detail=str(e))


    # Additional methods would be defined here, following the same documentation and structure.
"""
Define MongoAccountIn Model with fields:
    - name as string
    - number as string
    - owner_id as string representing ObjectId

Define MongoAccountOut Model with fields:
    - id as string
    - name as string
    - number as string
    - owner_id as string

Define MongoAccountQueries class inheriting from Queries with:
    - DB_NAME set to "twoshot"
    - COLLECTION set to "accounts"

Define method get_all_accounts_for_user with parameter owner_id:
    Convert owner_id to ObjectId
    Retrieve all documents from 'accounts' collection where 'owner_id' matches
    If retrieval is successful:
        Map each document to MongoAccountOut model
        Return list of MongoAccountOut models
    If retrieval fails:
        Raise HTTPException with status 500 and error details

Define method insert_account with parameter account_data:
    Convert account_data to dictionary
    Insert new document into 'accounts' collection
    If insertion is successful:
        Create MongoAccountOut model with inserted data
        Return MongoAccountOut model
    If insertion fails:
        Raise HTTPException with status 500 and error details

Define method get_account_by_id with parameter account_id:
    Convert account_id to ObjectId
    Retrieve document from 'accounts' collection by '_id'
    If account is found:
        Create MongoAccountOut model with account data
        Return MongoAccountOut model
    If account is not found:
        Raise HTTPException with status 404 and "Account not found" message
    If retrieval fails for other reasons:
        Raise HTTPException with status 500 and error details

Additional methods would follow a similar structure, tailored to their specific operations.

"""