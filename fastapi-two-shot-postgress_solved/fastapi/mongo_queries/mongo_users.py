"""
receipts.py

This module manages the data models and database queries for receipt operations within a MongoDB database. It uses PyMongo for database interaction and Pydantic for data validation and schema definition.

Classes:
    PyObjectId - Extends ObjectId to ensure valid ObjectId strings are used.
    MongoReceiptOut - A model for receipt data retrieved from the database.
    MongoReceiptIn - A model for validating receipt data before insertion into the database.
    UserInfo, CategoryInfo, AccountInfo - Models for user, category, and account information respectively.
    MongoReceiptOutAGG - A model for the aggregated output of receipt data, including related user, category, and account information.
    MongoReceiptQueries - Contains methods for CRUD operations on the 'receipts' collection, including complex aggregations to join related data.

Usage:
    Create an instance of MongoReceiptQueries to interact with the 'receipts' collection.
    Use MongoReceiptIn to validate new receipt data before insertion.
    Use MongoReceiptOut and MongoReceiptOutAGG to format receipt data retrieved from the database, with the latter providing a comprehensive view including related data.

Example:
    receipt_queries = MongoReceiptQueries()
    new_receipt_data = {
        'vendor': "Office Supplies Co.",
        'total': 123.45,
        'tax': 10.50,
        'purchaser_id': "507f1f77bcf86cd799439011",
        'category_id': "507f191e810c19729de860ea",
        'account_id': "507f1f77bcf86cd799439011"
    }
    new_receipt = MongoReceiptIn(**new_receipt_data)
    inserted_receipt = receipt_queries.create_receipt(new_receipt)
    print(inserted_receipt)
"""

from pydantic import BaseModel, EmailStr
from typing import Optional
from pymongo.errors import DuplicateKeyError, PyMongoError
from bson import ObjectId
from fastapi import HTTPException, status
from datetime import datetime
from mongo_queries.client import Queries  

class MongoUserIn(BaseModel):
    username: str 
    password: str  # Note: Password will be stored in plain text for this example
    first_name: str
    last_name: str
    email: EmailStr
    # The `date_joined` field is automatically set when creating a new user

class MongoUserOut(BaseModel):
    id: str
    username: str
    first_name: str
    last_name: str
    email: str
    last_login: Optional[datetime]
    date_joined: datetime

class DuplicateUserError(Exception):
    """Exception raised when an attempt is made to create a duplicate user."""

class MongoUserQueries(Queries):
    DB_NAME = "twoshot"
    COLLECTION = "users"

    def create(self, user_data: MongoUserIn) -> MongoUserOut:
        """
        Create a new user with the provided data.

        Args:
            user_data (UserIn): The user data to insert into the database.

        Returns:
            UserOut: The created user's data.

        Raises:
            DuplicateUserError: If a user with the same username already exists.
            HTTPException: If there is an unexpected error during insertion.
        """
        try:
            # Automatically set the date_joined field to the current timestamp
            user_data_dict = user_data.dict()
            user_data_dict['date_joined'] = datetime.utcnow()
            result = self.collection.insert_one(user_data_dict)
            # Retrieve the inserted user data to return
            return self.get_by_id(str(result.inserted_id))
        except DuplicateKeyError:
            raise DuplicateUserError("User with this username already exists.")
        except PyMongoError as e:
            raise HTTPException(status_code=500, detail=f"Database error: {str(e)}")

    def get_by_id(self, user_id: str) -> MongoUserOut:
        """
        Retrieve a user by their ID.

        Args:
            user_id (str): The ID of the user to retrieve.

        Returns:
            UserOut: The retrieved user's data.

        Raises:
            HTTPException: If the user is not found or there is an error during retrieval.
        """
        try:
            user = self.collection.find_one({"_id": ObjectId(user_id)})
            if user:
                # Convert MongoDB's '_id' to a string 'id'
                user['id'] = str(user.pop('_id'))
                return MongoUserOut(**user)
            else:
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail="User not found"
                )
        except PyMongoError as e:
            # Handle any other PyMongo errors
            raise HTTPException(status_code=500, detail=str(e))
        except Exception as e:
            # This captures any other exceptions, such as a ValueError from an invalid ObjectId
            raise HTTPException(status_code=400, detail=str(e))



    def get_all_users(self) -> list[MongoUserOut]:
        """
        Retrieve all users from the database.

        Returns:
            list[MongoUserOut]: A list of all user data.

        Raises:
            HTTPException: If there is an error during retrieval.
        """
        try:
            users_cursor = self.collection.find({})
            users_list = []
            for user in users_cursor:
                # Convert MongoDB's '_id' to a string 'id'
                user['id'] = str(user.pop('_id'))
                users_list.append(MongoUserOut(**user))
            return users_list
        except PyMongoError as e:
            raise HTTPException(status_code=500, detail=str(e))
        

    def delete(self, user_id: str) -> bool:
        pass
    # Additional CRUD operations would follow the same pattern of error handling and comments.

"""
Module: users.py

Description:
  Manages interactions with the 'users' collection in a MongoDB database.
  Validates data using Pydantic models and performs CRUD operations.

Dependencies:
  - Pydantic for data models
  - PyMongo for database interaction
  - ObjectId for MongoDB document identification
  - HTTPException for error handling

Models:
  PyObjectId:
    - Validates and converts ObjectId to string for use in models

  MongoUserIn:
    - Represents the structure of user data for insertion into the database
    - Includes fields for username, password, and personal details

  MongoUserOut:
    - Represents the structure of user data retrieved from the database
    - Includes fields for user id, username, and personal details

Classes:
  MongoUserQueries:
    - Contains methods to interact with the 'users' collection

Methods:
  create_user(user_data):
    - Validates user_data with MongoUserIn
    - Inserts validated user data into the database
    - Returns the inserted user data as MongoUserOut

  get_user_by_id(user_id):
    - Converts user_id to ObjectId
    - Retrieves user data from the database by ObjectId
    - Returns the user data as MongoUserOut
    - Handles user not found and database errors

  get_all_users():
    - Retrieves all user documents from the database
    - Returns a list of user data as MongoUserOut

  update_user(user_id, update_data):
    - Converts user_id to ObjectId
    - Validates update_data with MongoUserIn
    - Updates user data in the database
    - Returns the updated user data as MongoUserOut

  delete_user(user_id):
    - Converts user_id to ObjectId
    - Deletes the user document from the database
    - Handles user not found and database errors

Usage Example:
  - Instantiate MongoUserQueries
  - Create a new user using MongoUserIn
  - Insert the new user into the database
  - Retrieve and display the inserted user

Note:
  - Additional methods for user authentication and password management can be implemented following similar patterns.
"""