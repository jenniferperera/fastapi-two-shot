
"""
client.py

This module is responsible for establishing a connection to a MongoDB instance using the PyMongo library.
It utilizes an environment variable to securely access the MongoDB URI, which is used to instantiate a MongoClient object.
The MongoClient is then used throughout the application to interact with the database.

Classes:
    Queries - A class that encapsulates methods to perform database operations. It is designed to be subclassed
              by other classes that specify particular collections and databases to work with.

Usage:
    - This module is imported in other parts of the application where database operations are required.
    - The 'Queries' class is intended to be a base class, extended by other classes that define specific
      operations on the database collections.
    - The 'MONGO_URL' environment variable must be set in the environment for this module to function correctly.
      This URL should contain the credentials and address necessary to connect to the MongoDB instance.

Example:
    class UserQueries(Queries):
        DB_NAME = 'users_database'
        COLLECTION = 'users'

        def find_user(self, username):
            return self.collection.find_one({'username': username})

    # The UserQueries class can now be used to perform operations on the 'users' collection within the 'users_database'.
"""


import os
import pymongo



# Environment variable that stores the MongoDB connection string.
# This should be set in your environment or .env file and should not be hardcoded for security reasons.
MONGO_URL = os.environ["MONGO_URL"]

# Establish a connection to the MongoDB server using the connection string.
# The MongoClient instance represents the connection to the MongoDB server and is used to interact with the database.
client = pymongo.MongoClient(MONGO_URL)

class Queries:
    """
    The Queries class is a container for methods that perform database operations.
    This class abstracts the details of database access and provides a higher-level interface for database operations.
    
    Attributes:
        DB_NAME (str): The name of the database to interact with. Should be set by the subclass.
        COLLECTION (str): The name of the collection within the database to interact with. Should be set by the subclass.
        
    The `collection` property is a convenience property that provides access to the collection object.
    This allows for easy access to the collection for performing database operations.
    """

    @property
    def collection(self):
        # Access the database by the name provided in `DB_NAME`.
        # The `client` object is used to retrieve the database instance.
        db = client[self.DB_NAME]
        
        # Access the collection within the database by the name provided in `COLLECTION`.
        # The `db` object is used to retrieve the collection instance.
        # This collection instance is then used to perform operations like find, insert, update, and delete.
        return db[self.COLLECTION]

"""
1. Load the MongoDB URL from the environment variables.
   - This is a security measure to avoid hardcoding sensitive information in the code.

2. Establish a connection to MongoDB using the loaded URL.
   - This connection will be used for all interactions with the database.

3. Define a class named 'Queries' that will contain database operations.
   - This class will act as a template and should be subclassed for specific database and collection names.

4. Within the 'Queries' class, define a property named 'collection'.
   - This property is a shortcut to access the database and collection.
   - When accessed, it will:
     a. Connect to the specified database using the 'DB_NAME' attribute.
     b. Connect to the specified collection within that database using the 'COLLECTION' attribute.
   - This collection object can then be used to perform database operations like querying, inserting, updating, and deleting documents.

"""