"""
receipts.py

This module manages the data models and database queries for receipt operations within a MongoDB database. It uses PyMongo for database interaction and Pydantic for data validation and schema definition.

Classes:
    PyObjectId - Extends ObjectId to ensure valid ObjectId strings are used.
    MongoReceiptOut - A model for receipt data retrieved from the database.
    MongoReceiptIn - A model for validating receipt data before insertion into the database.
    UserInfo, CategoryInfo, AccountInfo - Models for user, category, and account information respectively.
    MongoReceiptOutAGG - A model for the aggregated output of receipt data, including related user, category, and account information.
    MongoReceiptQueries - Contains methods for CRUD operations on the 'receipts' collection, including complex aggregations to join related data.

Usage:
    Create an instance of MongoReceiptQueries to interact with the 'receipts' collection.
    Use MongoReceiptIn to validate new receipt data before insertion.
    Use MongoReceiptOut and MongoReceiptOutAGG to format receipt data retrieved from the database, with the latter providing a comprehensive view including related data.

Example:
    receipt_queries = MongoReceiptQueries()
    new_receipt_data = {
        'vendor': "Office Supplies Co.",
        'total': 123.45,
        'tax': 10.50,
        'purchaser_id': "507f1f77bcf86cd799439011",
        'category_id': "507f191e810c19729de860ea",
        'account_id': "507f1f77bcf86cd799439011"
    }
    new_receipt = MongoReceiptIn(**new_receipt_data)
    inserted_receipt = receipt_queries.create_receipt(new_receipt)
    print(inserted_receipt)
"""


from pydantic import BaseModel
from pymongo.collection import Collection
from pymongo.errors import PyMongoError
from bson import ObjectId
from fastapi import HTTPException
from datetime import datetime
from mongo_queries.client import Queries
from pydantic.error_wrappers import ValidationError

class PyObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not ObjectId.is_valid(v):
            raise ValueError('Invalid ObjectId')
        return str(v)  # Convert ObjectId to string

    @classmethod
    def __modify_schema__(cls, field_schema):
        field_schema.update(type='string')

class MongoReceiptOut(BaseModel):
    id: PyObjectId
    vendor: str
    total: float
    tax: float
    date: datetime
    purchaser_id: PyObjectId
    category_id: PyObjectId
    account_id: PyObjectId

class MongoReceiptIn(BaseModel):
    vendor: str
    total: float
    tax: float
    purchaser_id: str
    category_id: str
    account_id: str


class UserInfo(BaseModel):
    first_name: str
    email: str

class CategoryInfo(BaseModel):
    name: str

class AccountInfo(BaseModel):
    name: str

class MongoReceiptOutAGG(BaseModel):
    id: str 
    vendor: str
    total: float
    tax: float
    date: datetime
    purchaser_info: UserInfo
    category_info: CategoryInfo
    account_info: AccountInfo

    class Config:
            allow_population_by_field_name = True  # This allows us to use the field names directly


class MongoReceiptQueries(Queries):
    DB_NAME = "twoshot"
    COLLECTION = "receipts"

  
   
    
    def get_all_receipts_for_user(self, user_id: str) -> list[MongoReceiptOut]:
        try:
            # Convert the user_id string to ObjectId
            user_id_obj = ObjectId(user_id)

            # Define the aggregation pipeline
            pipeline = [
                {"$match": {"purchaser_id": user_id_obj}},  # Use ObjectId for matching
                {"$lookup": {  # Join with the users collection
                    "from": "users",
                    "localField": "purchaser_id",
                    "foreignField": "_id",
                    "as": "purchaser_info"
                }},
                {"$lookup": {  # Join with the categories collection
                    "from": "expensecategory",
                    "localField": "category_id",
                    "foreignField": "_id",
                    "as": "category_info"
                }},
                {"$lookup": {  # Join with the accounts collection
                    "from": "accounts",
                    "localField": "account_id",
                    "foreignField": "_id",
                    "as": "account_info"
                }},
                {"$project": {  # Define the structure of the output documents
                    "id": {"$toString": "$_id"},  # Convert _id to string and rename to id
                    "vendor": 1,
                    "total": 1,
                    "tax": 1,
                    "date": 1,
                    "purchaser_info": {"$arrayElemAt": ["$purchaser_info", 0]},
                    "category_info": {"$arrayElemAt": ["$category_info", 0]},
                    "account_info": {"$arrayElemAt": ["$account_info", 0]},
                }}
            ]

            # Execute the aggregation pipeline
            receipts_cursor = self.collection.aggregate(pipeline)
            receipts = [MongoReceiptOutAGG(**receipt) for receipt in receipts_cursor]
            return receipts
        except PyMongoError as e:
            raise HTTPException(status_code=500, detail=f"Database error: {e}")
        except Exception as e:
            raise HTTPException(status_code=400, detail=f"Error: {e}")

    def create_receipt(self, receipt_data: MongoReceiptIn) -> MongoReceiptOut:
        try:
            # Convert the data to a dictionary and update the date
            receipt_data_dict = receipt_data.dict()
            receipt_data_dict['date'] = datetime.utcnow()

            # Convert foreign key fields from string to ObjectId
            if 'purchaser_id' in receipt_data_dict:
                receipt_data_dict['purchaser_id'] = ObjectId(receipt_data_dict['purchaser_id'])
            if 'category_id' in receipt_data_dict:
                receipt_data_dict['category_id'] = ObjectId(receipt_data_dict['category_id'])
            if 'account_id' in receipt_data_dict:
                receipt_data_dict['account_id'] = ObjectId(receipt_data_dict['account_id'])

            # Insert the document into the collection
            result = self.collection.insert_one(receipt_data_dict)

            # Retrieve the inserted document using the ObjectId
            return self.get_receipt_by_id(result.inserted_id)
        except PyMongoError as e:
            raise HTTPException(status_code=500, detail=str(e))
        
    def get_receipt_by_id(self, receipt_id: ObjectId) -> dict:
        try:
            receipt = self.collection.find_one({"_id": receipt_id})
            if receipt:
                # Convert MongoDB's '_id' to a string 'id' inline
                receipt['id'] = str(receipt['_id'])
                return receipt
            else:
                raise HTTPException(status_code=404, detail="Receipt not found")
        except PyMongoError as e:
            raise HTTPException(status_code=500, detail=f"Database error: {e}")
        except Exception as e:
            # This captures any other exceptions, such as a ValueError from an invalid ObjectId
            raise HTTPException(status_code=400, detail=f"Error: {e}")

# Additional methods for update and delete operations can be added following the same pattern.
"""
Module: receipts.py

Description:
  Handles interactions with the 'receipts' collection in a MongoDB database.
  Validates data using Pydantic models and performs CRUD operations.

Dependencies:
  - Pydantic for data models
  - PyMongo for database interaction
  - ObjectId for MongoDB document identification
  - HTTPException for error handling
  - datetime for timestamping

Models:
  PyObjectId:
    - Validates and converts ObjectId to string

  MongoReceiptOut:
    - Represents the structure of receipt data retrieved from the database

  MongoReceiptIn:
    - Represents the structure of receipt data to be inserted into the database

  UserInfo, CategoryInfo, AccountInfo:
    - Represent structures for related user, category, and account data

  MongoReceiptOutAGG:
    - Represents the aggregated structure of receipt data including related entities

Classes:
  MongoReceiptQueries:
    - Contains methods to interact with the 'receipts' collection

Methods:
  get_all_receipts_for_user(user_id):
    - Converts user_id to ObjectId
    - Defines an aggregation pipeline to join 'receipts' with 'users', 'expensecategory', and 'accounts'
    - Executes the pipeline and returns a list of aggregated receipt data

  create_receipt(receipt_data):
    - Converts input data to a dictionary
    - Updates the 'date' field to the current time
    - Converts foreign key fields to ObjectId
    - Inserts the new receipt into the database
    - Retrieves and returns the inserted receipt by its ObjectId

  get_receipt_by_id(receipt_id):
    - Finds a receipt by its ObjectId
    - Converts '_id' to 'id' and returns the receipt data
    - Handles not found and database errors

Usage Example:
  - Instantiate MongoReceiptQueries
  - Create a new receipt using MongoReceiptIn
  - Insert the new receipt into the database
  - Retrieve and display the inserted receipt

Note:
  - Additional methods for updating and deleting receipts can be implemented following similar patterns.
"""