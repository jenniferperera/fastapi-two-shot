# Use Python 3.10 on Debian Bullseye as the base image
# This is a stable choice for most Python applications
FROM python:3.10-bullseye

# Upgrade pip to ensure the latest features and security fixes are available
RUN python -m pip install --upgrade pip

# Add a wait script to control the startup order in Docker Compose
# This script waits for the database to be ready before starting the application
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.9.0/wait /wait
RUN chmod +x /wait

# Create a directory for dependencies
# Isolating the dependency installation improves caching
WORKDIR /deps
COPY requirements.txt requirements.txt

# Install dependencies from the requirements file
# Docker can use cached layers for faster builds if requirements.txt doesn't change often
RUN python -m pip install -r requirements.txt

# Set the working directory for the application
WORKDIR /app

# Start the application using uvicorn with hot reloading enabled
# Hot reloading restarts the application automatically upon code changes, ideal for development
CMD /wait && uvicorn main:app --reload --host 0.0.0.0
