from fastapi import APIRouter, Depends, HTTPException
from queries.category import (
  CategoryOut,
  CategoryIn,
  CategoryQueries
)
from mongo_queries.mongo_category import (
  MongoCategoryQueries,
  MongoCategoryIn,
  MongoCategoryOut
)  
from datetime import date


router = APIRouter()





@router.get("/categories/{owner_id}", response_model=list[CategoryOut])
async def get_categories(
    owner_id: int,
    repo: CategoryQueries = Depends(),
):
     return repo.get_all_category_for_user(owner_id)


# @router.post("/categories/")
# async def create_category():
#     return "tbd"


# @router.get("/categories/{owner_id}", response_model=list[MongoCategoryOut])
# async def get_categories(
#     owner_id: str,  # The owner_id should be a string since it's an ObjectId in string form
#     repo: MongoCategoryQueries = Depends(),
# ) -> list[MongoCategoryOut]:
#       """
#       API endpoint to retrieve all categories associated with a given owner.

#       This endpoint will query the database for all categories linked to the owner ID provided in the path parameter. It returns a list of categories, each represented by a MongoCategoryOut object.

#       Parameters:
#           owner_id: The unique identifier of the owner, whose categories are to be retrieved, in string format.
#           repo: The repository instance to interact with the database.

#       Returns:
#           A list of MongoCategoryOut objects representing the owner's categories.

#       Raises:
#           HTTPException: If any error occurs during the database query, an HTTPException is raised with the appropriate status code and detail message.
#       """
    
#       return repo.get_all_category_for_user(owner_id)

# @router.post("/categories/", response_model=MongoCategoryOut)
# async def create_category(
#     category_in: MongoCategoryIn,
#     repo: MongoCategoryQueries = Depends(),
# )-> MongoCategoryOut:
#     """
#     API endpoint to create a new category.

#     This endpoint accepts category data, processes it, and inserts it into the database. It returns the created category as a MongoCategoryOut object.

#     Parameters:
#         category_in: The data for the new category to be created, as a MongoCategoryIn object.
#         repo: The repository instance to interact with the database.

#     Returns:
#         The created category as a MongoCategoryOut object.

#     Raises:
#         HTTPException: If any error occurs during the insertion process, an HTTPException is raised with the appropriate status code and detail message.
#     """
#     try:
#         created_category = repo.insert_category(category_in)
#         return created_category
#     except HTTPException as http_exc:
#         # If there's an HTTPException raised in the repo, pass it through
#         raise http_exc
#     except Exception as exc:
#         # For any other exceptions, return a generic error message
#         raise HTTPException(status_code=500, detail=str(exc))